
VIET TYPING MODULE
==================

This module allows users to type Vietnamese characters while creating Drupal nodes
or setting the site name or menu names... It allow to embed different Vietnamese
Input Engine (VIE) into website. If you don't know Vietnam, you are sure not to
need it. The instruction below therefore in Vietnamese.

Mô-đun này cho phép chèn các bộ gõ tiếng Việt khác nhau, như AVIM, Mudim... vào
website. Viet_typing tạo một khối để chọn kiểu gõ, bạn có thể hiển thị khối này ở
vị trí tùy thích. Mặc định khối chỉ hiển thị bạn ở trang sửa đổi.

Bạn cần tải các bộ gõ để dùng chung với mô-đun này:
- AVIM: http://sourceforge.net/projects/rhos/, phiên bản ngày 28/7/2008 trở về sau
- Mudim: http://code.google.com/p/mudim/, phiên bản bất kì

Sau khi tải về, bạn đặt bộ gõ vào thư mục sites/all/libraries, tên thư mục và
tên tập tin Javascript trùng với tên bộ gõ (thí dụ
sites/all/libraries/avim/avim.js). Nếu không tìm thấy, mô-đun sẽ tự động sử
dụng phiên bản mới nhất tại CDN miễn phí http://f.vtcdn.com/tools/avim/avim.js
